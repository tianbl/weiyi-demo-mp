package com.vsclouds.mp.community.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.vsclouds.mp.community.entity.CommunityVisitor;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CommunityVisitorMapper extends BaseMapper<CommunityVisitor> {

}