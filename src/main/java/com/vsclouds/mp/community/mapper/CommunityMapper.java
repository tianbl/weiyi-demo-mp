package com.vsclouds.mp.community.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.vsclouds.mp.community.entity.Community;

public interface CommunityMapper extends BaseMapper<Community> {

}