package com.vsclouds.mp.community.controller;

import com.vsclouds.mp.community.entity.CommunityVisitor;
import com.vsclouds.mp.util.CommonConstants;
import com.vsclouds.mp.util.ResponseData;
import com.vsclouds.mp.util.ResponseDataFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created with Software Dept.
 * <p> {@code UserController}
 *
 * @author : tianbaolei
 * @date : 2020-08-10 14:04
 * Description:
 */
@RestController
public class UserController {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @PostMapping(path = "/public/common/user/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseData createCommunity(@RequestBody Map<String, Object> body, HttpServletRequest request,
                                        HttpServletResponse response) {
        Long userId = (Long) request.getAttribute(CommonConstants.USER_ID);
        logger.info("[RequestMethod.POST /public/common/user/login ] userId={}", userId);
        Map<String, Object> result = new HashMap<>(8);
        result.put("token", UUID.randomUUID().toString());
        result.putAll(body);
        return ResponseDataFactory.success(response, result);
    }
}
