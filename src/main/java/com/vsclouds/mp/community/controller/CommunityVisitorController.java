package com.vsclouds.mp.community.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.vsclouds.mp.community.entity.CommunityVisitor;
import com.vsclouds.mp.community.thin.CommunityVisitorThin;
import com.vsclouds.mp.mp.entity.Solitaire;
import com.vsclouds.mp.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with Software Dept.
 * <p> {@code CommunityVisitorController}
 *
 * @author : tianbaolei
 * @date : 2020-08-06 17:00
 * Description:
 */
@RestController
public class CommunityVisitorController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private CommunityVisitorThin communityVisitorThin;

    @PostMapping(path = "/public/community/visitor/register", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseData createCommunity(@RequestBody CommunityVisitor visitor, HttpServletRequest request,
                                        HttpServletResponse response) {
        Long userId = (Long) request.getAttribute(CommonConstants.USER_ID);
        logger.info("[RequestMethod.POST /public/community/visitor/register ] userId={}", userId);
        int res = communityVisitorThin.insert(visitor);
        return ResponseDataFactory.success(response, res);
    }

    @GetMapping(path = "/community/visitor/list", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseData getSolitaireByPage(HttpServletRequest request, HttpServletResponse response) {

        Integer pageNum = CommonUtil.obj2Int(request.getParameter("pageNum"));
        Integer pageSize = CommonUtil.obj2Int(request.getParameter("pageSize"));

        Map<String, Object> params = new HashMap<>(8);
        params.put("from", CommonUtil.getFromByPageNum(pageNum, pageSize));
        params.put("count", pageSize == null ? CommonConstants.COUNT : pageSize);

        return ResponseDataFactory.success(response, communityVisitorThin.selectListByParams(params));
    }

    @GetMapping(path = "/community/visitor/list", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseData getSolitaireLast5MinuteList(HttpServletRequest request, HttpServletResponse response) {

        Integer pageNum = CommonUtil.obj2Int(request.getParameter("pageNum"));
        Integer pageSize = CommonUtil.obj2Int(request.getParameter("pageSize"));
        String unionid = request.getParameter("unionid");

        long endMillis = System.currentTimeMillis() + 300000;

        Map<String, Object> params = new HashMap<>(8);
        params.put("from", CommonUtil.getFromByPageNum(pageNum, pageSize));
        params.put("count", pageSize == null ? CommonConstants.COUNT : pageSize);
        params.put("unionid", unionid);
        params.put("endDate", new Date(endMillis));


        return ResponseDataFactory.success(response, communityVisitorThin.selectListByParams(params));
    }
}
