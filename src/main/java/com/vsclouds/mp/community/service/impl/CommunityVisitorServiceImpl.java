package com.vsclouds.mp.community.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.vsclouds.mp.community.entity.CommunityVisitor;
import com.vsclouds.mp.community.mapper.CommunityVisitorMapper;
import com.vsclouds.mp.community.service.CommunityVisitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created with Software Dept.
 * <p> {@code CommunityVisitorServiceImpl}
 *
 * @author : tianbaolei
 * @date : 2020-08-06 16:58
 * Description:
 */
@Service
public class CommunityVisitorServiceImpl implements CommunityVisitorService {

    @Autowired
    private CommunityVisitorMapper communityVisitorMapper;


    @Override
    public int insert(CommunityVisitor communityVisitor) {
        return communityVisitorMapper.insert(communityVisitor);
    }

    @Override
    public List<CommunityVisitor> selectListByParams(Map<String, Object> params) {
        QueryWrapper<CommunityVisitor> wrapper = new QueryWrapper<>();
        if (params.containsKey("from")) {
            wrapper.last("limit " + params.get("from") + "," + params.get("count"));
        }
        wrapper.orderByDesc("id");
        return communityVisitorMapper.selectList(wrapper);
    }
}
