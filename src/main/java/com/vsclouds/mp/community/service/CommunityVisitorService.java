package com.vsclouds.mp.community.service;

import com.vsclouds.mp.community.entity.CommunityVisitor;

import java.util.List;
import java.util.Map;

/**
 * Created with Software Dept.
 * <p> {@code CommunityVisitorService}
 *
 * @author : tianbaolei
 * @date : 2020-08-06 16:58
 * Description:
 */
public interface CommunityVisitorService {

    int insert(CommunityVisitor communityVisitor);

    List<CommunityVisitor> selectListByParams(Map<String, Object> params);
}
