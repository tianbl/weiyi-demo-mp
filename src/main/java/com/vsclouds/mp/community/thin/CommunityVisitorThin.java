package com.vsclouds.mp.community.thin;

import com.vsclouds.mp.community.entity.CommunityVisitor;

import java.util.List;
import java.util.Map;

/**
 * Created with Software Dept.
 * <p> {@code CommunityVisitorThin}
 *
 * @author : tianbaolei
 * @date : 2020-08-06 17:00
 * Description:
 */
public interface CommunityVisitorThin {

    int insert(CommunityVisitor communityVisitor);

    List<CommunityVisitor> selectListByParams(Map<String, Object> params);
}
