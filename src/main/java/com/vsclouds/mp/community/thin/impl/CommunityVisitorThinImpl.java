package com.vsclouds.mp.community.thin.impl;

import com.vsclouds.mp.community.entity.CommunityVisitor;
import com.vsclouds.mp.community.service.CommunityVisitorService;
import com.vsclouds.mp.community.thin.CommunityVisitorThin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created with Software Dept.
 * <p> {@code CommunityVisitorThinImpl}
 *
 * @author : tianbaolei
 * @date : 2020-08-06 17:00
 * Description:
 */
@Service
public class CommunityVisitorThinImpl implements CommunityVisitorThin {

    @Autowired
    private CommunityVisitorService communityVisitorService;

    @Override
    public int insert(CommunityVisitor communityVisitor) {
        return communityVisitorService.insert(communityVisitor);
    }

    @Override
    public List<CommunityVisitor> selectListByParams(Map<String, Object> params) {
        return communityVisitorService.selectListByParams(params);
    }
}
