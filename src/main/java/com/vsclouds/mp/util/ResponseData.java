package com.vsclouds.mp.util;

/**
 * Created with Software Dept.
 * <p> {@code ResponseData}
 *
 * @author : tianbaolei
 * @date : 2020-06-17 16:24
 * Description:
 */
public class ResponseData<T> {
    private String timestamp;

    private Integer status;

    private Integer resultCode;

    private String error;

    private String message;

    private String path;

    private T data;


    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getResultCode() {
        return resultCode;
    }

    public void setResultCode(Integer resultCode) {
        this.resultCode = resultCode;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
