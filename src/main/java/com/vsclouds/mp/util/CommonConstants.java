package com.vsclouds.mp.util;

/**
 * <p>
 *
 * @author tianbaolei  2019/2/17 13:44
 * @version 1.0
 */
//@Component
//@ConfigurationProperties(prefix = "com.lei.common")
public class CommonConstants {
    public static final String RESULT_CODE = "resultCode";

    public static final String USER_ID = "userId";

    /**
     * 用户类型 1-超级管理员 2-区域代理 3-培训学校用户 4-普通用户
     */
    public static final int USER_TYPE_SUPER = 1;
    public static final int USER_TYPE_AREA = 2;
    public static final int USER_TYPE_SCHOOL = 3;
    public static final int USER_TYPE_ORIDINARY = 4;

    /**
     * 状态 1 正常， 2 注册未激活， 3 冻结， 4 注销
     */
    public static final int USER_STATUS_NORMAL = 1;
    public static final int USER_STATUS_NO_ACTIVATE = 2;
    public static final int USER_STATUS_FROZEN = 3;
    public static final int USER_STATUS_CANCLE = 4;

    /**
     * 默认页码 1
     * 页大小   15
     */
    public static final int PAGE_NUM = 1;
    public static final int PAGE_SIZE = 15;

    public static final int FROM = 0;
    public static final int COUNT = 20;

//    public static String SERVER_URL;
//
//    public void setServerUrl(String serverUrl) {
//        CommonConstants.SERVER_URL = serverUrl;
//    }
}
