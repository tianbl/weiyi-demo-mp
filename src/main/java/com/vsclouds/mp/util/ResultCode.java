package com.vsclouds.mp.util;

/**
 * Created with Software Dept.
 * <p> {@code PasswordHelper}
 *
 * @author : tianbaolei
 * @date : 2020-06-19 13:15
 * Description:
 */
public enum ResultCode {

    /**
     * 成功
     */
    R_OPERATION_SUCCESS(0, "操作成功"),
    /**
     * 成功
     */
    R_INTERFACE_VERSION_CAN_NOT_HANDLER(1, "系统无法处理当前请求版本"),
    /**
     * aaa
     */
    R_TOKEN_INVALID(1000, "token无效，身份认证不通过"),
    /**
     * aaa
     */
    R_EMAIL_SEND_ERROR(1100, "邮件发送失败"),
    /**
     * aaa
     */
    R_SMS_SEND_ERROR(1101, "短信发送失败"),
    /**
     * aaa
     */
    R_USER_NOT_EXIST(1200, "用户不存在"),
    /**
     * aaa
     */
    R_USER_USERNAME_HAS_EXIST(1201, "用户名已经存在"),
    /**
     * aaa
     */
    R_USER_USERNAME_NOT_EXIST(1202, "用户名不存在"),
    /**
     * aaa
     */
    R_USER_EMAIL_HAS_EXIST(1203, "邮箱已经存在"),
    /**
     * aaa
     */
    R_USER_EMAIL_NOT_EXIST(1204, "邮箱不存在"),
    /**
     * aaa
     */
    R_USER_EMAIL_FORMAT_ERROR(1205, "邮箱格式错误"),
    /**
     * aaa
     */
    R_USER_MOBILE_FORMAT_ERROR(1206, "手机号格式错误"),
    /**
     * aaa
     */
    R_USER_MOBILE_NUMBER_HAS_EXIST(1210, "手机号已经存在"),
    /**
     * aaa
     */
    R_USER_MOBILE_NUMBER_NOT_EXIST(1211, "手机号不存在"),
    /**
     * aaa
     */
    R_USER_REGISTER_TYPE_ERROR(1212, "注册类型错误"),
    /**
     * aaa
     */
    R_VERIFICATION_CODE_TIME_OUT(1213, "验证码已过期"),
    /**
     * aaa
     */
    R_VERIFICATION_CODE_INVALID(1214, "验证码无效"),
    /**
     * aaa
     */
    R_USERNAME_PASSWORD_INVALID(1220, "用户名密码错误"),
    /**
     * aaa
     */
    R_USER_PROJECT_NOT_MATCH(1221, "用户项目不对应"),
    /**
     * aaa
     */
    R_USER_LOCKED(1222, "用户被锁定"),
    /**
     * aaa
     */
    R_USER_STATUS_ERROR(1223, "用户状态非正常，不允许登录"),
    /**
     * 微信未绑定手机号
     */
    R_USER_WEI_CHAT_NO_BIND_PHONE(1224, "当前授权登录微信未绑定手机号"),
    /**
     * aaa
     */
    R_BAD_REQUEST(4000, "请求错误"),
    /**
     * aaa
     */
    R_UNAUTHORIZED(4001, "身份认证不通过"),
    /**
     * aaa
     */
    R_FORBIDDEN(4003, "拒绝访问，无权限、参数错误、以及其他无法具体细分的错误都会导致拒绝访问"),
    /**
     * aaa
     */
    R_NOT_FOUND(4004, "接口不存在"),

    R_UNSUPPORTED_MEDIA_TYPE(4015, "Http Request Method Not Supported"),
    /**
     * aaa
     */
    R_RUNTIME_EXCEPTION(5000, "运行时服务器错误"),
    /**
     * aaa
     */
    R_SERVER_REDIS_ERROR(5001, "缓存服务错误"),
    /**
     * aaa
     */
    R_DUPLICATE_KEY_EXCEPTION(5002, "属性唯一约束错误，通常发生在重复添加信息的时候"),

    R_COURSE_TYPE_LIMIT(6010, "课程类型层级达到上限，最大50层"),

    R_WEI_MSG_ILLEGAL(87014, "内容含有违法违规内容");

    private Integer resultCode;
    private String description;

    ResultCode(int resultCode, String description) {
        this.resultCode = resultCode;
        this.description = description;
    }

    @Override
    public String toString() {
        return resultCode.toString();
    }

    public Integer value() {
        return resultCode;
    }

    public String getDescription() {
        return description;
    }

}
