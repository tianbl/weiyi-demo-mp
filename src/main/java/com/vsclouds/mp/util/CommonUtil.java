package com.vsclouds.mp.util;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * Created with Software Dept.
 * <p> {@code CommonUtil}
 *
 * @author : tianbaolei
 * @date : 2020-08-03 15:50
 * Description:
 */
public class CommonUtil {
    /**
     * 用于放置Long类型的可在线程内部共享变量,不可在线程间共享变量
     */
    private static ThreadLocal<Long> THREAD_LOCAL = new ThreadLocal<Long>();

//    /**
//     * 放置除projectId外的其他参数
//     * 请求结束后参数会被移除,目前仅用于前端请求
//     * 新增加的key值请在ThreadLocalConstant类中定义为常量
//     */
//    private static ThreadLocal<Map<String, Object>> mapThreadLocal = new ThreadLocal<>();

    public static void setThreadLocal(Long projectId) {
        THREAD_LOCAL.set(projectId);
    }

    public static Long gethreaLocal() {
        return THREAD_LOCAL.get();
    }

    public static int getFromByPageNum(Integer pageNum, Integer pageSize) {
        if (pageNum == null || pageSize == null) {
            return 0;
        }
        return (pageNum - 1) * pageSize;
    }

    public static String obj2String(Object obj) {
        if (obj instanceof String) {
            return (String) obj;
        }
        return obj == null ? null : obj.toString();
    }

    public static Integer obj2Int(Object obj) {
        if (obj instanceof Integer) {
            return (Integer) obj;
        }
        return obj == null ? null : Integer.parseInt(obj.toString());
    }

    public static Long obj2Long(Object obj) {
        if (obj instanceof Long) {
            return (Long) obj;
        }
        return obj == null ? null : Long.parseLong(obj.toString());
    }
}
