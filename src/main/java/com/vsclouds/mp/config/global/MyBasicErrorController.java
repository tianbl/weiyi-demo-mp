package com.vsclouds.mp.config.global;

import com.vsclouds.mp.util.ResponseData;
import com.vsclouds.mp.util.ResponseDataFactory;
import com.vsclouds.mp.util.ResponseHandler;
import com.vsclouds.mp.util.ResultCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorProperties;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * Created with Software Dept.
 * <p> {@code MyBasicErrorController}
 *
 * @author : tianbaolei
 * @date : 2019-03-07 10:35
 * Description: 暂时未使用
 */
public class MyBasicErrorController implements ErrorController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MyBasicErrorController.class);

    private ErrorAttributes errorAttributes;
    private ErrorProperties errorProperties;
    @Autowired
    private ServerProperties serverProperties;

    @Autowired
    public MyBasicErrorController(ErrorAttributes errorAttributes) {
        Assert.notNull(errorAttributes, "ErrorAttributes must not be null");
        this.errorAttributes = errorAttributes;
//        this.errorProperties = errorProperties;
    }


    /**
     * 返回错误页面路径
     *
     * @return
     */
    @Override
    public String getErrorPath() {
        LOGGER.info("==========================================");
        return "/error";
    }

    /**
     * http status=401 身份认证不通过
     */
    @RequestMapping(value = "/error")
    @ResponseBody
    public ResponseData error(HttpServletRequest request, HttpServletResponse response) {
        LOGGER.error("[{}.{}] error", request.getMethod(), request.getRequestURL());
        ResponseHandler.handle(response, HttpStatus.UNAUTHORIZED);
        return ResponseDataFactory.getResponseData(ResultCode.R_UNAUTHORIZED);
    }

    /**
     * http status=401 身份认证不通过
     */
    @RequestMapping(value = "401")
    @ResponseBody
    public ResponseData error401(HttpServletRequest request, HttpServletResponse response) {
        LOGGER.error("[{}.{}]请求身份认证失败", request.getMethod(), request.getRequestURL());
        ResponseHandler.handle(response, HttpStatus.UNAUTHORIZED);
        return ResponseDataFactory.getResponseData(ResultCode.R_UNAUTHORIZED);
    }

    /**
     * http status=404 api不存在
     */
    @RequestMapping(value = "404")
    @ResponseBody
    public ResponseData error404(HttpServletRequest request, HttpServletResponse response) {
        LOGGER.error("[{}.{}]请求接口不存在", request.getMethod(), request.getRequestURL());
        ResponseHandler.handle(response, HttpStatus.NOT_FOUND);
        return ResponseDataFactory.getResponseData(ResultCode.R_RUNTIME_EXCEPTION);
    }

    /**
     * Determine if the stacktrace attribute should be included.
     *
     * @param request  the source request
     * @param produces the media type produced (or {@code MediaType.ALL})
     * @return if the stacktrace attribute should be included
     */
    protected boolean isIncludeStackTrace(HttpServletRequest request,
                                          MediaType produces) {
        ErrorProperties.IncludeStacktrace include = this.serverProperties.getError().getIncludeStacktrace();
        if (include == ErrorProperties.IncludeStacktrace.ALWAYS) {
            return true;
        }
        if (include == ErrorProperties.IncludeStacktrace.ON_TRACE_PARAM) {
            return getTraceParameter(request);
        }
        return false;
    }


    /**
     * 获取错误的信息
     *
     * @param request
     * @param includeStackTrace
     * @return
     */
    private Map<String, Object> getErrorAttributes(HttpServletRequest request,
                                                   boolean includeStackTrace) {
        RequestAttributes requestAttributes = new ServletRequestAttributes(request);
        return this.errorAttributes.getErrorAttributes((WebRequest) requestAttributes, includeStackTrace);
    }

    /**
     * 是否包含trace
     *
     * @param request
     * @return
     */
    private boolean getTraceParameter(HttpServletRequest request) {
        String parameter = request.getParameter("trace");
        if (parameter == null) {
            return false;
        }
        return !"false".equals(parameter.toLowerCase());
    }

    /**
     * 获取错误编码
     *
     * @param request
     * @return
     */
    private HttpStatus getStatus(HttpServletRequest request) {
        Integer statusCode = (Integer) request
                .getAttribute("javax.servlet.error.status_code");
        if (statusCode == null) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        try {
            return HttpStatus.valueOf(statusCode);
        } catch (Exception ex) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }

}
