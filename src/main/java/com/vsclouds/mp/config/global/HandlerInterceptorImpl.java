package com.vsclouds.mp.config.global;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created with Software Dept.
 * <p> {@code HandlerInterceptorImpl}
 *
 * @author : tianbaolei
 * @date : 2020-03-05 8:06
 * Description:
 */
@Component
public class HandlerInterceptorImpl implements HandlerInterceptor {

//    @Autowired
//    private UserViewCache userViewCache;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
//        userViewCache.statisticsPv();
        return true;
    }
}
