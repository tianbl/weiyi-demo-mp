package com.vsclouds.mp.config.global;

import com.vsclouds.mp.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created with Software Dept.
 * <p> {@code GlobalExceptionHandler}
 *
 * @author : tianbaolei
 * @date : 2019-03-06 17:30
 * Description:
 */
@RestControllerAdvice
public class GlobalExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(value = Exception.class)
    public ResponseData defaultErrorHandler(HttpServletRequest request, HttpServletResponse response, Exception e) {
        Long userId = (Long) request.getAttribute(CommonConstants.USER_ID);
        LOGGER.error("[userId={} {}.{}]请求发生异常:{}", userId, request.getMethod(), request.getRequestURL(), e.getMessage());
        if (e instanceof NoHandlerFoundException) {
            ResponseHandler.handle(response, HttpStatus.NOT_FOUND, ResultCode.R_NOT_FOUND);
            return ResponseDataFactory.getResponseData(ResultCode.R_NOT_FOUND, e.getMessage());
        } else if (e instanceof HttpRequestMethodNotSupportedException) {
            ResponseHandler.handle(response, HttpStatus.UNSUPPORTED_MEDIA_TYPE, ResultCode.R_UNSUPPORTED_MEDIA_TYPE);
            return ResponseDataFactory.getResponseData(ResultCode.R_UNSUPPORTED_MEDIA_TYPE, e.getMessage());
        } else if (e instanceof MissingServletRequestParameterException || e instanceof HttpMessageNotReadableException) {
            ResponseHandler.handle(response, HttpStatus.BAD_REQUEST, ResultCode.R_BAD_REQUEST);
            return ResponseDataFactory.getResponseData(ResultCode.R_BAD_REQUEST, e.getMessage());
        } else if (e instanceof DuplicateKeyException) {
            ResponseHandler.handle(response, HttpStatus.BAD_REQUEST, ResultCode.R_DUPLICATE_KEY_EXCEPTION);
            return ResponseDataFactory.getResponseData(ResultCode.R_DUPLICATE_KEY_EXCEPTION, e.getMessage());
        } else {
            LOGGER.error("[{}.{}]请求发生异常:", request.getMethod(), request.getRequestURL(), e);
            ResponseHandler.handle(response, HttpStatus.INTERNAL_SERVER_ERROR, ResultCode.R_RUNTIME_EXCEPTION);
            return ResponseDataFactory.getResponseData(ResultCode.R_RUNTIME_EXCEPTION, e.getMessage());
        }
        // MySQLSyntaxErrorException 数据库错误
//        else if (e instanceof HttpMessageNotReadableException) {
//          参数错误，后续需要确定是否补充
//        }
    }
}
