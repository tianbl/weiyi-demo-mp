package com.vsclouds.mp.config.shiro.authc;

import org.apache.shiro.authc.*;
import org.apache.shiro.realm.AuthenticatingRealm;
import org.apache.shiro.util.ByteSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created with Software Dept.
 * <p> {@code UserRealm}
 *
 * @author : tianbaolei
 * @date : 2020-06-18 9:33
 * Description:
 */
public class UserRealm extends AuthenticatingRealm {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserRealm.class);

    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof UsernamePasswordToken;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        UsernamePasswordToken upToken = (UsernamePasswordToken) token;

        // SimplePrincipalCollection simplePrincipalCollection = new SimplePrincipalCollection();
        // simplePrincipalCollection.add("", getName());

        SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo("tianbaolei", "86858e97c09438ebfadcf0941cde0b54", getName());
        simpleAuthenticationInfo.setCredentialsSalt(ByteSource.Util.bytes("deae6bdda8539f86889a253276c846e7"));
        return simpleAuthenticationInfo;
    }
}
