package com.vsclouds.mp.config.shiro.authc;

import org.apache.shiro.web.filter.authc.AuthenticationFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 * Created with Software Dept.
 * <p> {@code StatelessAuthcFilter}
 *
 * @author : tianbaolei
 * @date : 2020-06-18 10:30
 * Description:
 */
public class StatelessAuthcFilter extends AuthenticationFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(StatelessAuthcFilter.class);


    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        // HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        // LOGGER.info("URL={}", ((HttpServletRequest) request).getRequestURL());
        return true;
    }

    @Override
    protected boolean onAccessDenied(ServletRequest servletRequest, ServletResponse servletResponse) throws Exception {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        LOGGER.info("URL={}", request.getRequestURL());
        return false;
    }
}
