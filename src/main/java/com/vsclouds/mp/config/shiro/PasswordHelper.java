package com.vsclouds.mp.config.shiro;

import org.apache.shiro.crypto.RandomNumberGenerator;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;

/**
 * Created with Software Dept.
 * <p> {@code PasswordHelper}
 *
 * @author : tianbaolei
 * @date : 2020-06-18 11:37
 * Description:
 */
public class PasswordHelper {
    private static RandomNumberGenerator randomNumberGenerator = new SecureRandomNumberGenerator();

    private static String algorithmName = "md5";

    private static final int hashIterations = 2;

    public static String eccryptPassword(String passwordSource) {
        return eccryptPassword(passwordSource, randomNumberGenerator.nextBytes().toHex());
    }

    public static String eccryptPassword(String passwordSource, String salt) {
        return new SimpleHash(algorithmName, passwordSource, ByteSource.Util.bytes(salt), hashIterations).toHex();
    }

}
