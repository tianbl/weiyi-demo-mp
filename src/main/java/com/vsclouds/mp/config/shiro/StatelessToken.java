package com.vsclouds.mp.config.shiro;

import org.apache.shiro.authc.AuthenticationToken;

import java.util.Date;

/**
 * Created with Software Dept.
 * <p> {@code StatelessToken}
 *
 * @author : tianbaolei
 * @date : 2020-06-19 17:45
 * Description:
 */
public class StatelessToken implements AuthenticationToken {

    private String token;

    private String userId;

    private Date createDate;

    @Override
    public Object getPrincipal() {
        return null;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
