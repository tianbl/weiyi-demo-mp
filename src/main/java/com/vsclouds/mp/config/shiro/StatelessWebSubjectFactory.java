package com.vsclouds.mp.config.shiro;

import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.SubjectContext;
import org.apache.shiro.web.mgt.DefaultWebSubjectFactory;

/**
 * Created with Software Dept.
 * <p> {@code StatelessWebSubjectFactory}
 *
 * @author : tianbaolei
 * @date : 2020-06-18 10:09
 * Description:
 */
public class StatelessWebSubjectFactory extends DefaultWebSubjectFactory {

    @Override
    public Subject createSubject(SubjectContext context) {
        // 禁止创建session
        context.setSessionCreationEnabled(false);
        return super.createSubject(context);
    }
}
