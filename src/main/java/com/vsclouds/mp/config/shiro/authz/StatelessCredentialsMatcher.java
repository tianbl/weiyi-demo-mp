package com.vsclouds.mp.config.shiro.authz;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.credential.CredentialsMatcher;

/**
 * Created with Software Dept.
 * <p> {@code StatelessCredentialsMatcher}
 *
 * @author : tianbaolei
 * @date : 2020-06-19 17:47
 * Description:
 */
public class StatelessCredentialsMatcher implements CredentialsMatcher {

    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
        return false;
    }
}
