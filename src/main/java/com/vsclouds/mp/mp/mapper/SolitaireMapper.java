package com.vsclouds.mp.mp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.vsclouds.mp.mp.entity.Solitaire;
import com.vsclouds.mp.mp.entity.vo.SolitaireVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface SolitaireMapper extends BaseMapper<Solitaire> {

//    Solitaire selectByaaa(@Param("id") Long id);

    @Select("select * from solitaire where id = #{id, jdbcType=BIGINT}")
    SolitaireVo selectVoById(@Param("id") Long id);
}