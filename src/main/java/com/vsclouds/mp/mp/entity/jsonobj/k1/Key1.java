package com.vsclouds.mp.mp.entity.jsonobj.k1;

import com.vsclouds.mp.mp.entity.jsonobj.Key;

/**
 * Created with Software IntelliJ IDEA
 * <p>
 *
 * @author Administrator  2019/9/15 14:05
 * @version 1.0
 */
public class Key1 extends Key {

    private String name;

    private String remark;

    private String remarkDescribe;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRemarkDescribe() {
        return remarkDescribe;
    }

    public void setRemarkDescribe(String remarkDescribe) {
        this.remarkDescribe = remarkDescribe;
    }

    @Override
    public String toString() {
        return "Key1{" +
                "name='" + name + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}
