package com.vsclouds.mp.mp.entity.jsonobj.k2;

import com.vsclouds.mp.mp.entity.jsonobj.Key;

import java.util.List;

/**
 * Created with Software IntelliJ IDEA
 * <p>
 *
 * @author Administrator  2019/9/15 14:05
 * @version 1.0
 */
public class Key2 extends Key {
    private List<Item> itemList;

    public List<Item> getItemList() {
        return itemList;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }

    @Override
    public String toString() {
        return "Key2{" +
                "itemList=" + itemList +
                '}';
    }
}
