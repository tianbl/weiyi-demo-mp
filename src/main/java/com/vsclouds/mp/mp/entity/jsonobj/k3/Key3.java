package com.vsclouds.mp.mp.entity.jsonobj.k3;

import com.vsclouds.mp.mp.entity.jsonobj.Key;

import java.util.List;

/**
 * Created with Software IntelliJ IDEA
 * <p>
 *
 * @author Administrator  2019/9/15 14:06
 * @version 1.0
 */
public class Key3 extends Key {

    /**
     * radio 单选框，checkbox 多选框
     */
    private String optionType;

    private List<Option> options;

    /**
     * 总票数
     */
    private Integer total;

    /**
     * 放弃数量或者空白投票数量
     */
    private Integer abandonCount;

    public String getOptionType() {
        return optionType;
    }

    public void setOptionType(String optionType) {
        this.optionType = optionType;
    }

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getAbandonCount() {
        return abandonCount;
    }

    public void setAbandonCount(Integer abandonCount) {
        this.abandonCount = abandonCount;
    }

    @Override
    public String toString() {
        return "Key3{" +
                "options=" + options +
                '}';
    }
}
