package com.vsclouds.mp.mp.entity.jsonobj;

import com.vsclouds.mp.mp.entity.jsonobj.k1.Key1;
import com.vsclouds.mp.mp.entity.jsonobj.k2.Key2;
import com.vsclouds.mp.mp.entity.jsonobj.k3.Key3;

import java.util.List;

/**
 * Created with Software IntelliJ IDEA
 * <p>  接龙配置信息实体类
 *
 * @author Administrator  2019/9/15 14:14
 * @version 1.0
 */

public class SolitaireConfig {

    /**
     * 报名接龙
     */
    private Key1 key1;

    /**
     * 信息采集接龙
     */
    private Key2 key2;

    /**
     * 投票选举
     */
    private Key3 key3;

    /**
     * 文件列表
     */
    private List filelist;

    public Key1 getKey1() {
        return key1;
    }

    public void setKey1(Key1 key1) {
        this.key1 = key1;
    }

    public Key2 getKey2() {
        return key2;
    }

    public void setKey2(Key2 key2) {
        this.key2 = key2;
    }

    public Key3 getKey3() {
        return key3;
    }

    public void setKey3(Key3 key3) {
        this.key3 = key3;
    }

    public List getFilelist() {
        return filelist;
    }

    public void setFilelist(List filelist) {
        this.filelist = filelist;
    }

    @Override
    public String toString() {
        return "ShiroConfig{" +
                "key1=" + key1 +
                ", key2=" + key2 +
                ", key3=" + key3 +
                '}';
    }
}
