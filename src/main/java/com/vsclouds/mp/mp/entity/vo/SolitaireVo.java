package com.vsclouds.mp.mp.entity.vo;

import com.vsclouds.mp.mp.entity.Solitaire;

/**
 * Created with Software Dept.
 * <p> {@code SolitaireVo}
 *
 * @author : tianbaolei
 * @date : 2020-08-05 17:09
 * Description:
 */
public class SolitaireVo extends Solitaire {

    private String aaaaa;

    public String getAaaaa() {
        return aaaaa;
    }

    public void setAaaaa(String aaaaa) {
        this.aaaaa = aaaaa;
    }
}
