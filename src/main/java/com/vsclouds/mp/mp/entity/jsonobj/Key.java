package com.vsclouds.mp.mp.entity.jsonobj;

/**
 * Created with Software IntelliJ IDEA
 * <p>
 *
 * @author Administrator  2019/9/15 14:04
 * @version 1.0
 */

public class Key {

    /**
     * 描述信息
     */
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Key{" +
                "description='" + description + '\'' +
                '}';
    }
}
