package com.vsclouds.mp.mp.entity.jsonobj.k3;

/**
 * Created with Software IntelliJ IDEA
 * <p>
 *
 * @author Administrator  2019/9/15 14:07
 * @version 1.0
 */

public class Option {

    /**
     * 标题
     */
    private String title;

    private String value;

    private String description;

    private String percentage;

    private Boolean checked = false;

    private int count;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Option{" +
                "title='" + title + '\'' +
                ", value='" + value + '\'' +
                ", percentage='" + percentage + '\'' +
                ", checked=" + checked +
                ", count=" + count +
                '}';
    }
}
