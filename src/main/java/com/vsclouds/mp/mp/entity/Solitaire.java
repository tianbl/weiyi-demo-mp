package com.vsclouds.mp.mp.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

@TableName(value = "solitaire")
public class Solitaire {

    @TableId(value = "id", type = IdType.INPUT)
    private Long id;

    @TableField(value = "user_id")
    private Long userId;

    @TableField(value = "title")
    private String title;

    /**
     * 1 报名， 2 信息采集，3 投票
     */
    @TableField(value = "type")
    private String type;

    /**
     * 状态：0 未发布，1  发布， 2 接龙结束,  3 过期, 4 未到开始时间
     */
    @TableField(value = "state")
    private Integer state;

    /**
     * 是否允许复制 0 不允许，1 允许，默认 0
     */
    @TableField(value = "copy")
    private Integer copy = 0;

    /**
     * 允许参与人数，0 表示没有限制，
     */
    @TableField(value = "members")
    private Integer members;

    /**
     * 浏览次数，放到内存中计算，数据库保留字段
     */
    @TableField(value = "views")
    private Integer views;

    @TableField(value = "is_all_visible")
    private Boolean isAllVisible;

    @TableField(value = "start_date")
    private Date startDate;

    @TableField(value = "end_date")
    private Date endDate;

    @TableField(value = "create_date")
    private Date createDate;

    @TableField(value = "update_date")
    private Date updateDate;

    @TableField(value = "content")
    private String content;

    @TableField(value = "json_field")
    private String jsonField;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getCopy() {
        return copy;
    }

    public void setCopy(Integer copy) {
        this.copy = copy;
    }

    public Integer getMembers() {
        return members;
    }

    public void setMembers(Integer members) {
        this.members = members;
    }

    public Integer getViews() {
        return views;
    }

    public void setViews(Integer views) {
        this.views = views;
    }

    public Boolean getAllVisible() {
        return isAllVisible;
    }

    public void setAllVisible(Boolean allVisible) {
        isAllVisible = allVisible;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getJsonField() {
        return jsonField;
    }

    public void setJsonField(String jsonField) {
        this.jsonField = jsonField;
    }

    @Override
    public String toString() {
        return "Solitaire{" +
                "id=" + id +
                ", userId=" + userId +
                ", title='" + title + '\'' +
                ", type='" + type + '\'' +
                ", state=" + state +
                ", copy=" + copy +
                ", members=" + members +
                ", views=" + views +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", createDate=" + createDate +
                ", updateDate=" + updateDate +
                ", content='" + content + '\'' +
                ", jsonField='" + jsonField + '\'' +
                '}';
    }
}