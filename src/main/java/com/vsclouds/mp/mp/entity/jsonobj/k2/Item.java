package com.vsclouds.mp.mp.entity.jsonobj.k2;

import com.vsclouds.mp.mp.entity.jsonobj.k3.Option;

import java.util.List;

/**
 * Created with Software Dept.
 * <p> {@code Item}
 *
 * @author : tianbaolei
 * @date : 2019-09-29 18:18
 * Description:
 */
public class Item {
    private String itemName;

    /**
     * input 输入框，radio 单选框，checkbox 多选框
     */
    private String itemType;

    /**
     * 保存信息值，多选信息保存
     */
    private String value;

    private String description;

    /**
     * 是否必填，默认false 表示不是必填
     */
    private Boolean mustFill = false;

    /**
     * radio 单选框，checkbox 多选框
     */
    private List<Option> options;

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getMustFill() {
        return mustFill;
    }

    public void setMustFill(Boolean mustFill) {
        this.mustFill = mustFill;
    }

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }

    @Override
    public String toString() {
        return "Item{" +
                "itemName='" + itemName + '\'' +
                ", itemType='" + itemType + '\'' +
                ", value='" + value + '\'' +
                ", mustFill=" + mustFill +
                '}';
    }
}
