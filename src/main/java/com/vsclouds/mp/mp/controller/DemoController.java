package com.vsclouds.mp.mp.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.vsclouds.mp.mp.entity.Solitaire;
import com.vsclouds.mp.mp.service.SolitaireService;
import com.vsclouds.mp.util.ResponseData;
import com.vsclouds.mp.util.ResponseDataFactory;
import com.vsclouds.mp.util.ResultCode;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with Software Dept.
 * <p> {@code DemoController}
 *
 * @author : tianbaolei
 * @date : 2020-06-11 14:57
 * Description:
 */
@RestController
public class DemoController {

    @Autowired
    private SolitaireService solitaireService;

    /**
     * HttpServletRequest request, HttpServletResponse response
     *
     * @return result
     */
    @GetMapping(path = "/demo", produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Object> demo() {

        Map<String, Object> result = new HashMap<>(4);
        result = null;
        result.put("result", "result");
        return result;
    }

    @GetMapping(path = "/solitaire/page", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseData<IPage> getSolitaireByPage(HttpServletRequest request, HttpServletResponse response) {

        IPage<Solitaire> page = solitaireService.selectByPage(1, 2);
        System.out.println(page.getRecords().size());
        return ResponseDataFactory.getResponseData(ResultCode.R_OPERATION_SUCCESS, page);
    }

    @PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseData<HashMap> login(@RequestBody Map<String, String> userVo, HttpServletRequest request, HttpServletResponse response) {
        String username = userVo.get("username");
        String password = userVo.get("password");

        Subject subject = SecurityUtils.getSubject();
        ResponseData<HashMap> resonseData = new ResponseData<>();
        HashMap<String, Object> result = new HashMap<>(8);
        try {
            UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(username, password);
            subject.login(usernamePasswordToken);

        } catch (IncorrectCredentialsException e) {
            System.out.println("用户名密码错误");
            resonseData.setData(result);
            return resonseData;
        } catch (ExcessiveAttemptsException e) {
            System.out.println("密码错误次数超过限定");
            resonseData.setData(result);
            return resonseData;
        } catch (Exception e) {
            System.out.println("登录失败");
            e.printStackTrace();
        }
        return resonseData;
    }

}
