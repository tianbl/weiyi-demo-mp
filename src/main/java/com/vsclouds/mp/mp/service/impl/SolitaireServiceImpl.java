package com.vsclouds.mp.mp.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.vsclouds.mp.mp.mapper.SolitaireMapper;
import com.vsclouds.mp.mp.entity.Solitaire;
import com.vsclouds.mp.mp.service.SolitaireService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created with Software IntelliJ IDEA
 * <p>
 *
 * @author Administrator  2019/9/15 13:25
 * @version 1.0
 */
@Service
public class SolitaireServiceImpl implements SolitaireService {

    @Autowired
    private SolitaireMapper solitaireMapper;

    @Override
    public Solitaire selectById(Long id) {
        return solitaireMapper.selectById(id);
    }

    @Override
    public int updateByPrimaryKeySelective(Solitaire record) {
        record.setUpdateDate(new Date());
        return solitaireMapper.updateById(record);
    }

    @Override
    public List<Solitaire> selectByParams(Map<String, Object> params) {
        QueryWrapper<Solitaire> wrapper = new QueryWrapper<>();
        Long userId = (Long) params.get("userId");
        wrapper.eq(userId != null, "user_id", userId);

        String title = (String) params.get("title");
        wrapper.eq(StringUtils.isNotEmpty(title), "title", title);

        // wrapper.inSql()

        return solitaireMapper.selectList(wrapper);
    }

    @Override
    public IPage<Solitaire> selectByPage(int pageNum, int pageSize) {
        // 参数一是当前页，参数二是每页个数
        IPage<Solitaire> page = new Page<>(pageNum, pageSize);
        QueryWrapper<Solitaire> wrapper = new QueryWrapper<>();
        return solitaireMapper.selectPage(page, wrapper);
    }

    @Override
    public int insert(Solitaire solitaire) {
        return solitaireMapper.insert(solitaire);
    }
}
