package com.vsclouds.mp.mp.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.vsclouds.mp.mp.entity.Solitaire;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * Created with Software IntelliJ IDEA
 * <p>
 *
 * @author Administrator  2019/9/15 13:25
 * @version 1.0
 */

public interface SolitaireService {

    Solitaire selectById(Long id);

    @Transactional
    int updateByPrimaryKeySelective(Solitaire record);

    List<Solitaire> selectByParams(Map<String, Object> params);

    IPage<Solitaire> selectByPage(int pageNum, int pageSize);

    int insert(Solitaire solitaire);

}
