package com.vsclouds.mp.test;

import com.vsclouds.mp.mp.entity.Solitaire;
import com.vsclouds.mp.mp.entity.vo.SolitaireVo;
import com.vsclouds.mp.mp.mapper.SolitaireMapper;
import com.vsclouds.mp.mp.service.SolitaireService;
import com.vsclouds.mp.util.JSONUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with Software Dept.
 * <p> {@code UserDaoTest}
 *
 * @author : tianbaolei
 * @date : 2020-06-02 14:37
 * Description:
 */
@RunWith(SpringRunner.class)
@SpringBootTest
//@MapperScan(basePackages = {"com.vsclouds.mp.mp.mapper"})
//@SpringBootConfiguration
public class UserDaoTest {

    @Autowired
    private SolitaireService solitaireService;
    @Autowired
    private SolitaireMapper solitaireMapper;

    @Test
    public void testSolitaire() {
        Map<String, Object> params = new HashMap<>();
        params.put("userId", 363L);
//        params.put("title", "的");
        List<Solitaire> solitaireList = solitaireService.selectByParams(params);
        if (solitaireList != null) {
            for (Solitaire solitaire : solitaireList) {
                System.out.println(solitaire.getId() + "\t" + solitaire.getUserId());
            }
        }
        System.out.println("solitaireList.size=" + solitaireList.size());
    }

    @Test
    public void testSolitaireUpdate() {
        Solitaire solitaire = solitaireService.selectById(365L);
        System.out.println(JSONUtil.toJson(solitaire));

        solitaire.setTitle("aaaaaaaaaaaaaaaaaaaaaaaaaa");
        solitaireService.updateByPrimaryKeySelective(solitaire);
    }


    @Test
    public void testTransactional() {
        Solitaire solitaire = new Solitaire();
        solitaire.setTitle("dfadfa");
        solitaire.setCreateDate(new Date());
        solitaire.setUpdateDate(new Date());
        solitaire.setAllVisible(true);
        int res = solitaireService.insert(solitaire);
        System.out.println("================>" + res);
    }

    @Test
    public void testAdd() {

    }

    @Test
    public void testExampleQuery() {
        SolitaireVo vo = solitaireMapper.selectVoById(365L);
        System.out.println(JSONUtil.toJson(vo));
    }
}
