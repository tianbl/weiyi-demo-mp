package com.example.demo;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with Software Dept.
 * <p> {@code JWTTest}
 *
 * @author : tianbaolei
 * @date : 2020-06-05 18:11
 * Description:
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class JWTTest {

    private static final String SECRET = "9a96349e2345385785e804e0f4254dee";

    private static String ISSUER = "sys_user";

    @Test
    public void testSolitaire() {

        //使用HMAC256进行加密
        Algorithm algorithm = Algorithm.HMAC256(SECRET);

        JWTCreator.Builder builder = JWT.create()
                .withIssuer(ISSUER)
                .withExpiresAt(new Date(System.currentTimeMillis() + 10 * 1000));

        Map<String, Long> claims = new HashMap<>();
        claims.put("userId", 1L);
        claims.forEach((k, v) -> {
            builder.withClaim(k, v);
        });
        System.out.println("============>" + builder.toString());
        String jwt = builder.sign(algorithm);
        System.out.println("jwt=" + jwt);

        decodeToken(jwt);
    }


    @Test
    public void verifierToken() {
        String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJzeXNfdXNlciIsImV4cCI6MTU5Mjk4NzQ0MiwidXNlcklkIjoxfQ.tZvrlKJvJs2IDxXJburSf0HVlyFLnQLKUo_0vnBHMZ4";
        try {
            checkToken(token);
        } catch (JWTVerificationException e) {
            //无效的签名/声明
            System.out.println("666");
            e.printStackTrace();
        }
    }

    private void decodeToken(String token) {
        DecodedJWT jwtDecoder = JWT.decode(token);

        System.out.println("jwt userId = " + jwtDecoder.getClaim("userId").asLong());
    }

    private void checkToken(String token) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(SECRET);
            JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer(ISSUER) //匹配指定的token发布者 auth0
                    .build();
            DecodedJWT jwt = verifier.verify(token); //解码JWT ，verifier 可复用

            System.out.println(jwt);
        } catch (JWTVerificationException e) {
            //无效的签名/声明
            System.out.println("666");
            e.printStackTrace();
        }
    }
}
