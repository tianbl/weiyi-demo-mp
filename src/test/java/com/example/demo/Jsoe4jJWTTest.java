//package com.example.demo;
//
//import org.apache.shiro.codec.Hex;
//import org.jose4j.json.JsonUtil;
//import org.jose4j.jwa.AlgorithmConstraints;
//import org.jose4j.jwk.RsaJsonWebKey;
//import org.jose4j.jwk.RsaJwkGenerator;
//import org.jose4j.jws.AlgorithmIdentifiers;
//import org.jose4j.jws.JsonWebSignature;
//import org.jose4j.jwt.JwtClaims;
//import org.jose4j.jwt.NumericDate;
//import org.jose4j.jwt.consumer.InvalidJwtException;
//import org.jose4j.jwt.consumer.JwtConsumer;
//import org.jose4j.jwt.consumer.JwtConsumerBuilder;
//import org.jose4j.keys.HmacKey;
//import org.jose4j.lang.JoseException;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.security.PrivateKey;
//
///**
// * Created with Software IntelliJ IDEA
// * <p> jwt 规范中字段解释
// * iss：jwt签发者
// * sub：jwt所面向的用户
// * aud：接收jwt的一方
// * exp：jwt过期时间，这个过期时间必须大于签发时间
// * nbf：定义在什么时间之前，这个jwt是不可用的
// * iat：jwt的签发时间
// * jti：jwt的唯一身份标识，用来所谓一次性token，从而避免重放攻击
// *
// * @author tianbaolei  2020/6/14 11:26
// * @version 1.0
// */
//@RunWith(SpringRunner.class)
//@SpringBootTest
//public class Jsoe4jJWTTest {
//
//    private static String keyId = "fa677d525c0e4ee485a61543937794af";
//    private static String privateKeyStr = "{\"kty\":\"RSA\",\"kid\":\"fa677d525c0e4ee485a61543937794af\"," +
//            "\"alg\":\"RS256\",\"n\":\"ki6NKW2ow53FBjWf21xNGF0v-Fzv9R4-vu5tz7LHz4vZ7TE2Lp7Xx0N2vFIH-HLZPLf" +
//            "AYW35W5iV29sW_MkbhVlh6f0q4AeCIeYrVjBGbcYTK5g-Sb8i9sO78DkGivryKTU4tUnOjqar2bfobwXScFhAgc4-BjIc" +
//            "vZ9V8LEzcAW76he500-sqekXqvYv7LbxIMlbadGEwbBqjscE83hiYjk1KSFrEeNWKP6E0X_cHVGEEGys8IKlBcfwOOCgaJ" +
//            "0sCFxvN3M54V33jSUknFzHAi1qJRsOI87-Fk1oYS-aniQOTfm5y5x1syTIgWEX9JvXCQgTxjp2kMItuoL2G2faoQ\"," +
//            "\"e\":\"AQAB\",\"d\":\"OmSUCN-AEZv9LwzOrW6CcWAQIHLne4-4WsadYOE2hcaEqAYHcboL0dI2JOXTv0AJXQK9u2" +
//            "2VtSwPeMJcvV-MOclJnpF9xf3Z0rbByuz_xSvhToHDJ-xNCCuJ8FynK28wuptC6s_vzfXwIckf9PFrbWsjYXbEOe9cobZ" +
//            "7Ould9boHkEq-x0eKkDFfcQbevuQeM54FCk5FGl_D4wpbuqudZiT8FYCJPO7m-MTnND6xzdEVuApuQUPCjAin34ygp9QP" +
//            "k2VfQaM1z-ZHchTaXtsv5gyLwu3bqA91vugmrtGuSBG8pFaN6mYL_ivt1Q_7QDLqXZ5_WBhkoXO5DZ4-6ApLvQ\"," +
//            "\"p\":\"58fTzOMjU1zJ42ZdUkJjSpRlZXdlWx3ILStrvVw7kCHH1iBZ4Lckd7SdqJ9DUfdIBFTNPaAKfDqpoz7fWOUZy" +
//            "yUpy7_8qaDagFi53pAjuCGUuKnOV5hbSPBIOvGi9iTHwLNz6Om5eRV6lBXoTRJgT7g8u88HeSyl29RmYt-N7rs\"," +
//            "\"q\":\"oXTxPC0BFDxpFjk2Jpuj6hBkioD3Kiv7HEJjxRoOL3TEAAc3811f7V7bO5FWK2L_2g8I5YiZhsyh0SyxjP" +
//            "BfuIzcbAfGSTAsFqKiIOGZ7fqiFTOV319FJIkcMOrT2dbvGxNeGgTJpWB1vcX6C8i3ytzPFto1H9Bl3xAPVFflHFM\"," +
//            "\"dp\":\"RU-eaLCryav_u37K_WRY6N6Di9oudxbq24cWiuPf8_QGHGREPEzIHPvoAZrOuN4nrRPm5DzNpeStAeI1TBIG" +
//            "qpcMbp-U4Oz3KlZeDs4vwEpafPZafBtVgPRJxUapIs5Q5bFEQixSiIEBzPLYKuQJ5Q0FLGx2oafWWWykyYBsoy0\"," +
//            "\"dq\":\"SS2_yQ581rcqyi_UI1uXx5b2evBJFowonH5ayhMtKsU5sOmUqnE_8U50_2K4M6IDZMo7tg1byIUnMq-XKd" +
//            "IpEHSH008SyElVMk00PsMCCaL3o7Rl0YBUzmJ2rJVCwBFy_kqg9BoHazV1KDZ7RqwK4Z-DHVB5k5nZEmktCYVtCpE\"," +
//            "\"qi\":\"jDbVP3xGaYx2wgAGJT7KNjdoC4dYbl5ajp9saRCgAih3TYr1CjZSrLRm9L90UukgwajU0pHaffP74epVx0R" +
//            "BhnS5GtZhCoGitpLwYSDkZ9qTMTVnFPHrg6M1OYEEXjZ_UKlnYCrzrDe6tfHcO1UTzMzsuOgHjRAV7IS6ImfzwVw\"}";
//    private static String publicKeyStr = "{\"kty\":\"RSA\",\"kid\":\"fa677d525c0e4ee485a61543937794af\"," +
//            "\"alg\":\"RS256\",\"n\":\"ki6NKW2ow53FBjWf21xNGF0v-Fzv9R4-vu5tz7LHz4vZ7TE2Lp7Xx0N2vFIH-HL" +
//            "ZPLfAYW35W5iV29sW_MkbhVlh6f0q4AeCIeYrVjBGbcYTK5g-Sb8i9sO78DkGivryKTU4tUnOjqar2bfobwXScFhA" +
//            "gc4-BjIcvZ9V8LEzcAW76he500-sqekXqvYv7LbxIMlbadGEwbBqjscE83hiYjk1KSFrEeNWKP6E0X_cHVGEEGys8IK" +
//            "lBcfwOOCgaJ0sCFxvN3M54V33jSUknFzHAi1qJRsOI87-Fk1oYS-aniQOTfm5y5x1syTIgWEX9JvXCQgTxjp2kMItuo" +
//            "L2G2faoQ\",\"e\":\"AQAB\"}";
//
//
//    @Test
//    public void commonTokenCreater() {
//
//        JwtClaims claims = new JwtClaims();
////        claims.setGeneratedJwtId();
////        claims.setIssuedAtToNow();
//        // expire date
//        NumericDate date = NumericDate.now();
//        date.addSeconds(60);
//
//        claims.setExpirationTime(date);
//
//        claims.setSubject("subject");
//        claims.setAudience("audience");
//        // 设定自定义参数，必须是字符串
//        claims.setClaim("account", "account");
//
//        JsonWebSignature jws = new JsonWebSignature();
//        jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.HMAC_SHA256);
//        // jws.setKeyIdHeaderValue(keyId);
//        jws.setPayload(claims.toJson());
//        try {
//
//            System.out.println("==============jws-payload=======>" + claims.toJson());
//
//            // PrivateKey privateKey = new RsaJsonWebKey(JsonUtil.parseJson(privateKeyStr)).getPrivateKey();
//            byte[] bytes = Hex.decode(keyId + keyId);
//            System.out.println(bytes.length);
//            HmacKey hmacKey = new HmacKey(bytes);
//            jws.setKey(hmacKey);
//            // jws.setDoKeyValidation(false);
//            String idToken = jws.getCompactSerialization();
//
//            System.out.println("==============idToken=======>" + idToken);
//            checkJwt(idToken);
//        } catch (JoseException e) {
//            e.printStackTrace();
//        }
//    }
//
//
//    @Test
//    public void checkJwt() {
//        String jwt = "eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1OTI5ODczNzgsInN1YiI6InN1YmplY3QiLCJhdWQiOiJhdWRpZW5jZSIsImFjY291bnQiOiJhY2NvdW50In0.mQc3QMM0vUTtFzMgs_d2RLC75ouj_00frTESRZSg1os";
//        checkJwt(jwt);
//    }
//
//    private void checkJwt(String jwt) {
//
//        JwtConsumer jwtConsumer = new JwtConsumerBuilder().setRequireExpirationTime()
//                .setAllowedClockSkewInSeconds(30)
//                .setRequireSubject()
////                .setExpectedIssuer("issuer")
//                .setExpectedAudience("audience")
//                .setVerificationKey(new HmacKey(Hex.decode(keyId + keyId)))
//                .setJweAlgorithmConstraints(new AlgorithmConstraints(AlgorithmConstraints.ConstraintType.WHITELIST, AlgorithmIdentifiers.HMAC_SHA256))
//                .build();
//
//        try {
//            JwtClaims jwtClaims = jwtConsumer.processToClaims(jwt);
//        } catch (InvalidJwtException e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Test
//    public void getInstance() {
//        // 生成一个RSA密钥对，用于签署和验证JWT，包装在JWK中
//        try {
//            RsaJsonWebKey rsaJsonWebKey = RsaJwkGenerator.generateJwk(2048);
//            rsaJsonWebKey.setKeyId("jwt1");
//            rsaJsonWebKey.getPrivateKey();
//            System.out.println(Hex.encodeToString(rsaJsonWebKey.getPrivateKey().getEncoded()));
//        } catch (JoseException e) {
//            e.printStackTrace();
//        }
//    }
//
//
//}
