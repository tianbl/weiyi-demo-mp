package com.example.demo;

import com.vsclouds.mp.config.shiro.PasswordHelper;
import org.apache.shiro.codec.Hex;
import org.apache.shiro.crypto.AesCipherService;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.DefaultHashService;
import org.apache.shiro.crypto.hash.HashRequest;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.util.ByteSource;
import org.apache.shiro.util.SimpleByteSource;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.security.Key;

/**
 * Created with Software Dept.
 * <p> {@code ShiroTest}
 *
 * @author : tianbaolei
 * @date : 2020-06-12 11:29
 * Description:
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ShiroTest {
    @Test
    public void ShiroHex() {
        String str = "hello";
        String salt = "123";
        String md5 = new Md5Hash(str, salt, 2).toString();
        System.out.println(md5);
    }

    @Test
    public void testSHA512() {
        DefaultHashService hashService = new DefaultHashService(); //默认算法SHA-512
        hashService.setHashAlgorithmName("SHA-512");
        hashService.setPrivateSalt(new SimpleByteSource("123")); //私盐，默认无
        hashService.setGeneratePublicSalt(true);//是否生成公盐，默认false
        hashService.setRandomNumberGenerator(new SecureRandomNumberGenerator());//用于生成公盐。默认就这个
        hashService.setHashIterations(1); //生成Hash值的迭代次数
        HashRequest request = new HashRequest.Builder()
                .setAlgorithmName("MD5").setSource(ByteSource.Util.bytes("hello"))
                .setSalt(ByteSource.Util.bytes("123")).setIterations(2).build();
        String hex = hashService.computeHash(request).toHex();
        System.out.println("===========SHA-512=======>" + hex);
    }

    @Test
    public void randomSecureRandomNumberGenerator() {
        SecureRandomNumberGenerator randomNumberGenerator = new SecureRandomNumberGenerator();
        // randomNumberGenerator.setSeed();
        String hex = randomNumberGenerator.nextBytes().toHex();
        System.out.println("===========random-hex=======>" + hex);
        System.out.println(PasswordHelper.eccryptPassword("tianbaolei", hex));
        System.out.println("System.getproperty(“java.io.tmpdir”) == " + System.getProperty("java.io.tmpdir"));
    }

    @Test
    public void aesTest() {
        AesCipherService aesCipherService = new AesCipherService();
        aesCipherService.setKeySize(128); //设置key长度
        //生成key
        Key key = aesCipherService.generateNewKey();
        System.out.println("===========key=======>" + key.toString());
        String text = "hello";
        //加密
        String encrptText = aesCipherService.encrypt(text.getBytes(), key.getEncoded()).toHex();
        System.out.println("===========encrptText=======>" + encrptText);
        //解密
        String text2 = new String(aesCipherService.decrypt(Hex.decode(encrptText), key.getEncoded()).getBytes());
        System.out.println("===========text2=======>" + text2);
        Assert.assertEquals(text, text2);
    }
}
